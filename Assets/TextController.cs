using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TextController : MonoBehaviour
{
    private Vector3 originalSuccessScale, originalFailScale;
    [SerializeField]
    private RectTransform success, wrong;
    private void OnEnable()
    {
        GameManager.Fail += popUpFailAnim;
        GameManager.Success += popUpSuccessAnim;
    }
    private void OnDisable()
    {
        GameManager.Fail -= popUpFailAnim;
        GameManager.Success -= popUpSuccessAnim;
    }
    // Start is called before the first frame update
    void Start()
    {
        originalSuccessScale = success.localScale;
        originalFailScale = wrong.localScale;
        success.localScale = Vector3.zero;
        wrong.localScale = Vector3.zero;
    }
    void popUpFailAnim()
    {
        wrong.transform.DOScale(originalFailScale, 1f).SetEase(Ease.Flash).OnComplete(() =>
        {
            wrong.transform.DOScale(Vector3.zero, 1f);
            GameManager.instance.setTheGameUp();
        });
    }
    void popUpSuccessAnim()
    {

        success.transform.DOScale(originalSuccessScale, 1f).SetEase(Ease.Flash).OnComplete(() =>
        {
            success.transform.DOScale(Vector3.zero, 1f);
            GameManager.instance.setTheGameUp();
        });
    }

}

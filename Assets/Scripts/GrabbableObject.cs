using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GrabbableObject : MonoBehaviour
{
    private Vector3 originalPos;
    private Vector3 originalRot;

    private bool isGrabbed = false;
    private RaycastHit hit;
    private float grabHeight = 2.5f;
    private void OnEnable()
    {
        GameManager.Grabbed += Grabbed;
        GameManager.Dropped += Dropped;
    }

    private void OnDisable()
    {
        GameManager.Grabbed -= Grabbed;
        GameManager.Dropped -= Dropped;

    }
    void Awake()
    {
        originalPos = transform.position;
        originalRot = transform.rotation.eulerAngles;
        gameObject.tag = "Grabbable";
    }

    void Update()
    {
        if (isGrabbed)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            if (Physics.Raycast(ray, out hit))
            {
                Vector3 targetPos = new Vector3(hit.point.x, grabHeight, hit.point.z);
                transform.position = targetPos;
            }
        }
    }

    private void Grabbed(int id)
    {
        if (id == gameObject.GetInstanceID())
        {
            isGrabbed = true;
        }
    }

    private void Dropped(int id)
    {
        if (id == gameObject.GetInstanceID())
        {
            isGrabbed = false;
            returnToOriginalPos();
        }
    }
    public void returnToOriginalPos()
    {
        transform.DOMove(originalPos,1f);
        transform.DORotate(originalRot,1f);
    }
}

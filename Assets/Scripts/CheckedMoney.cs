using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CheckedMoney : MonoBehaviour
{
    [SerializeField]
    private bool isGenuine;
    [SerializeField]
    private GameObject fakePositive;    //fake money model for UV light
    [SerializeField]
    private GameObject fakeNegative;    //genuine money model for UV light
    [SerializeField]
    private GameObject doodlePositive;  //doodled money model for the glass
    [SerializeField]
    private GameObject doodleNegative;  //genuine money model for the glass
    private bool colliderSafetyNet = false;
    void Awake()
    {
        if (Random.Range(0f,1f) > .5f)
        {
            isGenuine = true;
            fakeNegative.SetActive(true);
            doodleNegative.SetActive(true);
        }
        else
        {
            isGenuine = false;
            setCounterfeits();
        }
    }

    private void setCounterfeits()
    {
        if (Random.Range(0f, 1f) > .5f)   //both impurities are present
        {
            doodlePositive.SetActive(true);
            fakePositive.SetActive(true);
        }
        else
        {
            if (Random.Range(0f, 1f) > .5f)   //only one of the impurities are present
            {

                doodlePositive.SetActive(true);
                fakeNegative.SetActive(true);
            }
            else
            {
                fakePositive.SetActive(true);
                doodleNegative.SetActive(true);
            }
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (!colliderSafetyNet) //so that the collision doesnt happen twice
        {
            colliderSafetyNet=true;
            if (other.gameObject.CompareTag("Shredder"))
            {
                GameManager.instance.shred();
                Destroy(gameObject);
            }
            else if (other.gameObject.CompareTag("MoneyPile"))
            {
                GameManager.instance.putOnPile();
                Destroy(gameObject);
            }
        }
    }

    public bool IsGenuine()
    {
        return isGenuine;
    }
}

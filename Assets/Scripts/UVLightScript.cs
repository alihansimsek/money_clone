using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class UVLightScript : MonoBehaviour
{
    public GameObject lightPanel;
    private Vector3 grabRotation = new Vector3(-45, -35, 100);

    private void OnEnable()
    {
        GameManager.Grabbed += Grabbed;
        GameManager.Dropped += Dropped;
    }

    private void OnDisable()
    {
        GameManager.Grabbed -= Grabbed;
        GameManager.Dropped -= Dropped;

    }
    private void Grabbed(int id)
    {
        if(id == gameObject.GetInstanceID())
        {
            transform.DORotate(grabRotation, .5f);
            turnOnTheLight();
        }
    }

    private void Dropped(int id)
    {
        if (id == gameObject.GetInstanceID())
        {
            turnOffTheLight();
        }
    }

    public void turnOnTheLight()
    {
        lightPanel.SetActive(true);
    }
    public void turnOffTheLight()
    {
        lightPanel.SetActive(false);
    }
}

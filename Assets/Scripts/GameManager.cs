using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public GameObject checkedMoney;
    public MoneyPileController pile;
    public ShredderController shredder;
    public Transform spawnPoint;

    private float offset = 5f;
    private GameObject holdObject = null;
    private bool isGenuine;

    public delegate void GrabObject(int gameObjectID);
    public static event GrabObject Grabbed;
    public static event GrabObject Dropped;
    public delegate void DecisionEvent();
    public static event DecisionEvent Success;
    public static event DecisionEvent Fail;
    private void Awake()
    {
        if (!instance)
            instance = this;
        else
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }
    void Start()
    {
        setTheGameUp();
    }

    public void setTheGameUp()
    {
        isGenuine = spawnMoney();
    }

    private bool spawnMoney()
    {
        GameObject currentMoney = Instantiate(checkedMoney, spawnPoint.transform.position, Quaternion.Euler(0f,90f,0f));
        currentMoney.transform.position = currentMoney.transform.position + (Vector3.back * offset);
        currentMoney.transform.DOMove(spawnPoint.position, 1f);
        return currentMoney.GetComponent<CheckedMoney>().IsGenuine();
    }

    public void attachObjectToTouch(GameObject holdObject)
    {
        if (holdObject.gameObject.CompareTag("Grabbable"))
        {
            holdObject.GetComponent<Collider>().enabled = false;
            this.holdObject = holdObject;
            if(Grabbed != null)
                Grabbed(holdObject.GetInstanceID());
        }
    }
    public void dropObjectFromTouch()
    {
        if(holdObject != null){
            holdObject.GetComponent<Collider>().enabled = true;
            if(Dropped != null)
                Dropped(holdObject.GetInstanceID());
            holdObject = null;
        }
    }
    public void shred()
    {
        shredder.shredAnim();
        if (isGenuine)
        {
            if (Fail != null)
                Fail();
        }
        else
        {
            if (Success != null)
                Success();
        }
    }

    public void putOnPile()
    {
        pile.pileAnim();
        if (isGenuine)
        {
            if (Success != null)
                Success();
        }
        else
        {
            if (Fail != null)
                Fail();
        }
    }
}

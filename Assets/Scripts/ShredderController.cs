using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ShredderController : MonoBehaviour
{
    private Animator animator;
    [SerializeField]
    private GameObject shredMoney;
    private Vector3 shredMoneyOriginalPos;
    // Start is called before the first frame update
    void Start()
    {
        shredMoneyOriginalPos = shredMoney.transform.position;
        shredMoney.SetActive(false);
        animator = GetComponent<Animator>();
    }
    
    public void shredAnim()
    {
        shredMoney.SetActive(true);
        animator.SetTrigger("MoneyShredStart");
        shredMoney.transform.DOLocalMoveX(0f, 1.5f).OnComplete(() =>
        {
            shredMoney.SetActive(false);
            shredMoney.transform.position = shredMoneyOriginalPos;
        });
    }
}
